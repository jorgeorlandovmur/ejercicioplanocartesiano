
package Negocio;

import Modelo.Funcion;
import ufps.util.varios.ArchivoLeerURL;
//import ufps.util.varios.ArchivoLeerURL;



public class PlanoCartesiano {
    
    
    private String urlPuntos;
    private Funcion funciones[];

    public PlanoCartesiano() {
    }

    
    public PlanoCartesiano(String urlPuntos) {
        ArchivoLeerURL archivo=new ArchivoLeerURL("https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/coordenascartesianas/-/raw/master/puntos.csv");
        Object datos[]=archivo.leerArchivo();
        //No se toma en cuenta la primera línea del archivo , ya que está contiene el formato de los datos
        //Crear los espacios para las funciones:
        this.funciones=new Funcion[datos.length-1];
        //índice del arreglo de las funciones
        int i=0;
        for(int filasArchivo=1;filasArchivo<datos.length;filasArchivo++)
        {
            this.funciones[i++]=new Funcion(datos[filasArchivo].toString());
        }
        
        this.ordenar();
        
    }

    @Override
    public String toString() {
        
        String msg="";
        for(Funcion myFuncion:this.funciones)
            msg+=myFuncion.toString()+"\n";
        
        return msg;
    }

    
    
    
    
    
    public Funcion[] getFunciones() {
        return funciones;
    }

    public void setFunciones(Funcion[] funciones) {
        this.funciones = funciones;
    }
    
    /**
     * Ordena las funciones SEGÚN LA CANTIDAD DE PUNTOS , DE FORMA DESCENDENTE
     * ES DECIR, EL ELEMENTO 0 TENDRÁ LA FUNCIÓN CON MAS PUNTOS , Y ASÍ SUCESIVAMENTE
     * para esté ordenamiento , utilizar el método burbuja mejorado
     * ver más información en: http://www.bufoland.cl/tc/burbuja2.php
     */
    public void ordenar()
    {
        
        // :)
    }

    
}
