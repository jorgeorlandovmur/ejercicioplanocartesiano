/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.PlanoCartesiano;

/**
 *
 * @author Jorge Orlando
 */
public class PruebaPlanoCartesiano {
    
    public static void main(String[] args) {
        PlanoCartesiano plano=new PlanoCartesiano("https://gitlab.com/jorgeorlandovmur/coordenascartesianas/-/raw/master/puntos.csv");
        //PlanoCartesiano plano=new PlanoCartesiano("https://gitlab.com/danielomartb/ejemplo-csv/-/raw/master/EjemploPlanoCartesiano.csv");
        //ArchivoLeerURL archivo=new ArchivoLeerURL("https://gitlab.com/danielomartb/ejemplo-csv/-/raw/master/EjemploPlanoCartesiano.csv");
        System.out.println("Mi plano cartesiano Ordenado es:\n"+plano.toString());
    }
    
}
