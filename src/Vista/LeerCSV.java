/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import ufps.util.varios.*;


/**
 *
 * @author Jorge Orlando
 */
public class LeerCSV {
    public static void main(String [] args) {
        
///////////////////////////////////////////////////        

        String cadena = "D:\\Jorge orlando\\Documents\\NetBeansProjects\\ESTRUCTURAS DE DATOS\\ejercicioplanocartesiano-master\\puntos.csv";
        String elemento=null;
        boolean seguir = true;
        Scanner entrada = null;

        try {
            entrada = new Scanner(new File(cadena));
        }
        catch (FileNotFoundException fnE) {
            System.out.println(fnE.getMessage());
            seguir = false;
        }

        if (seguir) {
            entrada.nextLine();
            while (entrada.hasNext()) {
                elemento=entrada.next();
                System.out.println(elemento);
            }
        }
       
//////////////////////////////////////////////////     
/**
BufferedReader rd = new BufferedReader( new FileReader ("D:\\Jorge orlando\\Documents\\NetBeansProjects\\ESTRUCTURAS DE DATOS\\ejercicioplanocartesiano-master\\puntos.csv"));
String line = null;
ArrayList lista = new ArrayList();
String[] linea;
while ((line = rd.readLine()) != null) {
    line = rd.readLine();
    System.out.println(line);
    linea = line.split(";");
    lista.add(linea[3]);
}
*/      
////////////////////////////////////////////////       
/**        
        String fileName= "D:\\Jorge orlando\\Documents\\NetBeansProjects\\ESTRUCTURAS DE DATOS\\ejercicioplanocartesiano-master\\puntos.csv";
        File file= new File(fileName);

        // this gives you a 2-dimensional array of strings
        List<List<String>> lines = new ArrayList<>();
        Scanner inputStream;

        try{
            inputStream = new Scanner(file);

            while(inputStream.hasNext()){
                String line= inputStream.next();
                String[] values = line.split(";");
                // this adds the currently parsed line to the 2-dimensional string array
                lines.add(Arrays.asList(values));
            }

            inputStream.close();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // the following code lets you iterate through the 2-dimensional array
        int lineNo = 1;
        for(List<String> line: lines) {
            int columnNo = 1;
            for (String value: line) {
                System.out.println("Line " + lineNo + " Column " + columnNo + ": " + value);
                columnNo++;
            }
            lineNo++;
        }
*/
//////////////////////////////////////////////////
/**        
    public List<String[]> readData() throws IOException { 
    int count = 0;
    String file = "D:\\Jorge orlando\\Documents\\NetBeansProjects\\ESTRUCTURAS DE DATOS\\ejercicioplanocartesiano-master\\puntos.csv";
    List<String[]> content = new ArrayList<>();
    try(BufferedReader br = new BufferedReader(new FileReader(file))) {
        String line = "";
        while ((line = br.readLine()) != null) {
            content.add(line.split(","));
        }
    } catch (FileNotFoundException e) {
      //Some error logging
    }
    return content;
} 
*/
//////////////////////////////////////////////////
/**
Scanner scan = new Scanner(new File("D:\\Jorge orlando\\Documents\\NetBeansProjects\\ESTRUCTURAS DE DATOS\\ejercicioplanocartesiano-master\\puntos.csv"));
ArrayList<String[]> records = new ArrayList<String[]>();
String[] record = new String[2];
while(scan.hasNext())
{
    record = scan.nextLine().split(",");
    records.add(record);
}
//now records has your records.
//here is a way to loop through the records (process)
    for(String[] temp : records)
    {
        for(String temp1 : temp)
        {
            System.out.print(temp1 + " ");
        }
        System.out.print("\n");
    }
*/
//////////////////////////////////////////////////
        
    }    
}
