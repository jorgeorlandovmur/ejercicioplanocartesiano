
package Vista;

import Modelo.Punto;
import javax.swing.JOptionPane;

/**
 *
 * @author Jorge Orlando
 */
public class Prueba_punto_captura_Obligatorio {
    
    public static void main(String[] args) {
       
        
         String datos=JOptionPane.showInputDialog(null, "Digite PUNTOS SEPARADOS POR ,","Puntos", JOptionPane.QUESTION_MESSAGE);
         try{
            Punto p1=new Punto(datos);
            JOptionPane.showMessageDialog(null, "El punto fue:"+p1.toString());
         }catch(Exception e)
         {
             JOptionPane.showMessageDialog(null, e.getMessage());
         }
    }
}
