
package Vista;
import ufps.util.varios.*;

/**
 *
 * @author Jorge Orlando
 */
public class PruebaLeerURL {
    
    public static void main(String[] args) {
        //ArchivoLeerURL archivo=new ArchivoLeerURL("https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/coordenascartesianas/-/raw/master/puntos.csv");
        //ArchivoLeerURL archivo=new ArchivoLeerURL("https://gitlab.com/jorgeorlandovmur/coordenascartesianas/-/raw/master/puntos.csv");
        ArchivoLeerURL archivo=new ArchivoLeerURL("http://www.madarme.co");
        Object datos[]=archivo.leerArchivo();
        int i=0;
        for(Object filas:datos)
            System.out.println("Filas["+(i++)+"]:\t"+filas.toString());
    }
    
}
