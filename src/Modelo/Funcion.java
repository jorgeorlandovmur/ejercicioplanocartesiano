
package Modelo;

import Modelo.Punto;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jorge Orlando
 */
public class Funcion implements Comparable{
    
    private Punto puntos[];

    public Funcion() {
    }
    
    public Funcion(String datos) {
        String datosFila[]=datos.split(";");
        // Crear el arreglo de puntos
        this.puntos=new Punto[datosFila.length];
        for(int j=0;j<this.puntos.length;j++)
        {
            try {
                this.puntos[j]=new Punto(datosFila[j]);
            } catch (Exception ex) {
                System.out.println("No se puede crear el punto:"+ex.getMessage());
            }
        }

        //this.ordenarPuntos();
        
    }

    @Override
    public String toString() {
        String msg="";
        for(Punto myPunto:this.puntos)
            msg+=myPunto.toString()+"\t";

        return msg;
    }
    
    /**
     * Ordena el vector de puntos usando el método de la burbuja
     * ver más información: https://runestone.academy/runestone/static/pythoned/SortSearch/ElOrdenamientoBurbuja.html
     * Ordena ascendentemente por el eje X (tomar en cuenta todas las normas de nuestra matemáticas)
     */
    
    private void ordenarPuntos(Punto puntos[])
    {
        Punto aux;
       for(int i = 0 ; i < (puntos.length - 1) ; i++){
            for (int j = 0; j < (puntos.length - 1); j++) {
                
                // T hereda de object
                
                Punto dato2 = puntos[j+1];
                // CompareTo --> Comparable
                int comparador = ((Comparable)(puntos[j])).compareTo(dato2); 
                if(comparador == 1){
                    aux = puntos[j];
                    puntos[j] = puntos[j+1];
                    puntos[j+1] = aux;
                }
            }
        }
            // :)
    }
    
    

    public Punto[] getPuntos() {
        return puntos;
    }

    public void setPuntos(Punto[] puntos) {
        this.puntos = puntos;
    }

    @Override
    public int compareTo(Object obj) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        final Funcion other = (Funcion) obj;
        int variable=0;
        
        if(this.puntos.length<other.puntos.length){
            variable=1;
        }
        
        return variable;
    }
    
    
    
}
